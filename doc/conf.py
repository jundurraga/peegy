import sys
import os
import toml


def reset_mpl(gallery_conf, fname):
    return None


sys.path.insert(0, os.path.abspath('..'))
# sys.path.append("../")
with open('../pyproject.toml', 'r') as f:
    config = toml.load(f)
__version__ = config['project']['version']

rst_epilog = '.. |version| replace:: {:}'.format(__version__)
project = 'pEEGy'
author = u'Jaime A. Undurraga'
version = __version__
copyright = open('../LICENSE.txt').read()
release = __version__

# sphinx-gallery configuration
sphinx_gallery_conf = {
    'doc_module': os.path.join('peegy'),
    'backreferences_dir': os.path.join('generated'),
    'doc_module': 'peegy',
    'reset_modules': (reset_mpl),
    'download_all_examples': False,
    'show_memory': True,
    'filename_pattern': os.path.join('/example_'),
    'capture_repr': ('_repr_html_', '__repr__'),
    'examples_dirs': ['../examples'],
    'gallery_dirs': ['auto_examples'],
    'line_numbers': False,
    'plot_gallery': 'True',
}

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx_gallery.gen_gallery',
    "sphinx_multiversion",
    'sphinx.ext.autosectionlabel',
    'alabaster',
    'sphinx.ext.mathjax',
]

autosummary_generate = True
numfig = True
autosectionlabel_prefix_document = True
autosectionlabel_maxdepth = 2

# theme settings
html_title = 'pEEGy'
html_short_title = 'pEEGy'

html_static_path = ["_static"]
html_css_files = [
    'style.css',
]
html_theme_options = {
    'logo': 'peegy.png',
    'logo_name': True,
    'page_width': 'auto',
    'body_max_width': 'auto',
    'github_user': 'sphinx-doc',
    'github_repo': 'alabaster',
}
html_sidebars = {
    '**': ['about.html',
           'navigation.html',
           'searchbox.html'
           ]
 }

# settings for sphinx-multiversion
smv_tag_whitelist = r'^v\d+\.\d+\.\d+$'  # Include tags like "v2.1.1"
smv_remote_whitelist = None
imgmath_image_format = 'svg'
imgmath_use_preview = True  # to get correct alignment with surrounding inline text
templates_path = ['_templates']
