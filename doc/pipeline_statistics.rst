:orphan:

Statistics
============

.. py:currentmodule:: peegy

.. toctree::

Specific statistics pipeline processing InputOutput classes
------------------------------------------------------------

.. py:currentmodule:: peegy
.. automodule:: peegy.processing.pipe.statistics
   :no-members:
   :no-inherited-members:

.. autosummary::
   :toctree: generated/

   FTest
   HotellingT2Test
   PhaseLockingValue
   Covariance
