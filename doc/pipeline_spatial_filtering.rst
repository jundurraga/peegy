:orphan:

Spatial filtering
=================

.. currentmodule:: peegy

.. toctree::

Specific spatial filtering pipeline processing InputOutput classes
---------------------------------------------------------------------

.. py:currentmodule:: peegy
.. automodule:: peegy.processing.pipe.spatial_filtering
   :no-members:
   :no-inherited-members:

.. autosummary::
   :toctree: generated/

   CreateAndApplySpatialFilter
   ApplySpatialFilter
   SpatialFilter
   PlotSpatialFilterComponents
   ProjectSpatialComponents
   ProjectSpatialComponents
